//
//  SampleTestSuite.swift
//  XCUI RobotsUITests
//
//  Created by Ferdinand Goeldner on 28.08.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import XCTest

class SampleTestSuite: XCTestCase {
    
    lazy var chatRobot = ChatScreenRobot()
    
    var messages = MessageConstants.messages
    
    override func setUp() {
        super.setUp()
    }
    
    func test_onPage() {
        chatRobot
            .checkPageTitle()
    }
    
    func test_textFieldInteractable() {
        chatRobot
            .tapMessageField()
            .checkKeyboardIsOpen()
    }
    
    func test_messageSend() {
        chatRobot
            .tapMessageField()
            .checkKeyboardIsOpen()
            .typeMessage(message: "Hello World")
            .hitSendButton()
    }
    
    func test_sendMultipleMessages() {
        chatRobot
            .tapMessageField()
            .checkKeyboardIsOpen()
            .sendMessages(messages: messages)
    }

    func test_removeAllEntries() {
        chatRobot
            .tapMessageField()
            .sendMessages(messages: messages)

        for message in messages {
            let element = chatRobot.findLabel(withString: message)
            chatRobot.tap(element)
        }

        
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

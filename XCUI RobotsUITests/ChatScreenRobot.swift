//
//  ChatScreenRobot.swift
//  XCUI RobotsUITests
//
//  Created by Ferdinand Goeldner on 28.08.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import XCTest

class ChatScreenRobot: Robot {
    lazy private var pageTitle = Ids.screenTitle

    @discardableResult
    func checkPageTitle() -> Self {
        assertExists(navigationBar)
        return self
    }

    @discardableResult
    func tapMessageField() -> Self {
        tap(searchField)
        return self
    }

    @discardableResult
    func checkKeyboardIsOpen() -> Self {
        assertKeyboardShown()
        return self
    }

    @discardableResult
    func typeMessage(message: String) -> Self {
        searchField.typeText(message)
        return self
    }

    @discardableResult
    func hitSendButton() -> Self {
        keyboards.buttons[Ids.send].tap()
        return self
    }

    @discardableResult
    func sendMessage(message: String) -> Self {
        typeMessage(message: message)
        hitSendButton()
        return self
    }

    @discardableResult
    func sendMessages(messages: [String]) -> Self {
        for message in messages {
            sendMessage(message: message)
        }
        return self
    }

    func findLabel(withString: String) -> XCUIElement {
        return app.staticTexts[withString]
    }
}

extension ChatScreenRobot {
    struct Ids {
        static var screenTitle: String {
            return "Talk to yourself."
        }
        
        static var send: String {
            return "Senden"
        }
    }
}


extension ChatScreenRobot {
    var navigationBar: XCUIElement {
        return app.navigationBars[Ids.screenTitle]
    }
    
    var keyboards: XCUIElementQuery {
        return app.keyboards
    }
    
    var searchField: XCUIElement {
        return app.searchFields.firstMatch
    }
}

//
//  BadTestExample.swift
//  XCUI RobotsUITests
//
//  Created by Ferdinand Goeldner on 07.11.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import XCTest

class BadTestExample: XCTestCase {
    var app = XCUIApplication()
    var messages = MessageConstants.messages

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testBadTestExample() {
        // Launch App
        app.launch()

        // Tap Textfield
        app.searchFields.firstMatch.tap()

        // Check if keyboard appears
        let expectation = XCTNSPredicateExpectation(predicate: NSPredicate(format: "count > 0"), object: app.keyboards)
        guard XCTWaiter.wait(for: [expectation], timeout: 5.0) == .completed else {
            XCTFail("Keyboard is not shown.")
            return
        }

        for message in messages {
            // Enter Text
            app.searchFields.firstMatch.typeText(message)

            // Send Text
            app.keyboards.buttons["Senden"].tap()
        }

        for message in messages {
            XCTAssertTrue(app.staticTexts[message].exists)
            app.staticTexts[message].tap()
        }

        for i in 0...5 {
            sleep(1)
            app.swipeUp()
        }
    }
}

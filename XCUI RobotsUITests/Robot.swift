//
//  Robot.swift
//  XCUI RobotsUITests
//
//  Created by Ferdinand Goeldner on 28.08.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import XCTest

class Robot {
    var app = XCUIApplication()
    
    init() {
        app.launch()
    }
    
    func tap(_ element: XCUIElement, timeout: TimeInterval = 5) {
        let expectation = XCTNSPredicateExpectation(predicate: NSPredicate(format: "isHittable == true"), object: element)
        guard XCTWaiter.wait(for: [expectation], timeout: timeout) == .completed else {
            XCTAssert(false, "Element \(element.label) not hittable")
            return
        }
        
        element.tap()
    }
    
    func assertExists(_ elements: XCUIElement..., timeout: TimeInterval = 5) {
        for element in elements {
            XCTAssertTrue(element.exists, "Element doesn't exist.")
        }
    }
    
    func assertKeyboardShown(timeout: TimeInterval = 5) {
        let expectation = XCTNSPredicateExpectation(predicate: NSPredicate(format: "count > 0"), object: app.keyboards)
        guard XCTWaiter.wait(for: [expectation], timeout: timeout) == .completed else {
            XCTAssert(false, "Keyboard is not shown.")
            return
        }
    }
}

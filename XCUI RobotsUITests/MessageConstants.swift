//
//  MessageConstants.swift
//  XCUI RobotsUITests
//
//  Created by Ferdinand Goeldner on 06.11.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import Foundation

class MessageConstants {
    static var messages = ["Hey fellow jambitees.",
                           "Hope you're paying attention.",
                           "Not reading mindless texts!",
                           "Seriously though, you're great!"]
}

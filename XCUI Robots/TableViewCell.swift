//
//  TableViewCell.swift
//  XCUI Robots
//
//  Created by Ferdinand Goeldner on 28.08.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var elementLabel: UILabel!
    
    public var elementText: String {
        get {
            return elementLabel.text ?? ""
        } set(value) {
            elementLabel.text = value
            elementLabel.accessibilityIdentifier = value
        }
    }
}

//
//  ViewController.swift
//  XCUI Robots
//
//  Created by Ferdinand Goeldner on 28.08.19.
//  Copyright © 2019 Ferdinand Goeldner. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var elements: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBar.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension ViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "element") as? TableViewCell else {
            return UITableViewCell()
        }
        
        cell.elementText = elements[indexPath.item]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        elements.remove(at: indexPath.item)
        tableView.reloadData()
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard searchBar.text != "" && searchBar.text != nil else {
            return
        }
        elements.append(searchBar.text!)
        searchBar.text = ""
        
        tableView.reloadData()
    }
}

